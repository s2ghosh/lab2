/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110war
 *
 */
public class Celsius extends Temperature
{
	public Celsius (float t)
	{
		super(t);
		
	}
	
	public String toString ()
	{
		String s = "";
		s += getValue();
		s += " C";
		return s;
	}

	@Override
	public Temperature toCelsius() {
		return this;
	}

	@Override
	public Temperature toFahrenheit() {
		float celsius = this.getValue() ;
		float fahrenheit = (float)((((float)9/5) * celsius) + 32);
		return new Fahrenheit(fahrenheit);

	}

	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		float celsius = this.getValue();
		float kelvin = (float)(celsius + (float) 273.15);
		return new Kelvin(kelvin);
	}

	
	
}
