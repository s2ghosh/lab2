package edu.ucsd.cs110w.tests;

import junit.framework.TestCase;
import edu.ucsd.cs110w.temperature.Celsius;
import edu.ucsd.cs110w.temperature.Fahrenheit;
import edu.ucsd.cs110w.temperature.Temperature;

public class FahrenheitTests extends TestCase {
	private float delta = 0.001f;
	public void testFahrenheit(){
		float value = 12.34f;
		Fahrenheit temp = new Fahrenheit(value);
		assertEquals(value, temp.getValue(), delta);
	}
	public void testFahrenheitToString(){
		float value = 12.34f;
		Fahrenheit temp = new Fahrenheit(value);
		String string = temp.toString();
		String beginning = "" + value;
		String ending = " F";
		assertTrue(string.startsWith(beginning)); 
		assertTrue(string.endsWith(ending));
		int endIndex = string.indexOf(ending);
		assertTrue(string.substring(0, endIndex).equals(beginning));
	}
	public void testFahrenheitToFahrenheit()
	{
		Fahrenheit temp = new Fahrenheit(0);
		Temperature convert = temp.toFahrenheit();
		assertEquals(0, convert.getValue(), delta);
	}
	public void testFahrenheitToCelsius()
	{
		Fahrenheit temp = new Fahrenheit(32);
		Temperature convert = temp.toCelsius();
		assertEquals(0, convert.getValue(), delta);
		temp = new Fahrenheit(212);
		convert = temp.toCelsius();
		assertEquals(100, convert.getValue(), delta);
	}
	public void testFahrenheitToKelvin()
	{
		Fahrenheit temp = new Fahrenheit(32);
		Temperature convert = temp.toKelvin();
		assertEquals(273.15, convert.getValue(), delta);
		temp = new Fahrenheit(212);
		convert = temp.toKelvin();
		assertEquals(373.15, convert.getValue(), delta);
		temp = new Fahrenheit ((float)479.93);
		convert = temp.toKelvin();
		assertEquals (522,convert.getValue(),delta);
		
	}
}
