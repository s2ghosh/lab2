/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110war
 *
 */
public class Fahrenheit extends Temperature 
{
	public Fahrenheit (float t)
	{
		super(t);
		
	}
	
	public String toString ()
	{
		String s = "";
		s += Float.toString(this.getValue());
		s += " F";
		return s;
	}

	@Override
	public Temperature toCelsius() {
		float fahrenheit = this.getValue();
		float celsius = (float) ((fahrenheit - (float)(32)) * ((float)5/9));
		return new Celsius(celsius);
		
	}

	@Override
	public Temperature toFahrenheit() {
		
		return this;
	}

	@Override
	public Temperature toKelvin() {
		float fahrenheit = this.getValue();
		float celsius = (float) ((fahrenheit - (float)(32)) * ((float)5/9));
		float kelvin = (float) (celsius + 273.15);
		return new Kelvin (kelvin);
	}

	
	
}
