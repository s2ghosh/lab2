package edu.ucsd.cs110w.tests;

import edu.ucsd.cs110w.temperature.Kelvin;
import edu.ucsd.cs110w.temperature.Temperature;
import junit.framework.TestCase;

public class KelvinTests extends TestCase {
	private float delta = 0.001f;
	public void testKelvin(){
		float value = 12.34f;
		Kelvin temp = new Kelvin(value);
		assertEquals(value, temp.getValue(), delta);
	}
	public void testKelvinToString(){
		float value = 12.34f;
		Kelvin temp = new Kelvin(value);
		String string = temp.toString();
		String beginning = "" + value;
		String ending = " K";
		assertTrue(string.startsWith(beginning)); 
		assertTrue(string.endsWith(ending));
		int endIndex = string.indexOf(ending);
		assertTrue(string.substring(0, endIndex).equals(beginning));
	}
	public void testKelvinToCelsius()
	{
		Kelvin temp = new Kelvin((float)273.15);
		Temperature convert = temp.toCelsius();
		assertEquals((float)0, convert.getValue(), delta);
	}
	public void testKelvinToFahrenheit()
	{
		Kelvin temp = new Kelvin((float)273.15);
		Temperature convert = temp.toFahrenheit();
		assertEquals((float)32, convert.getValue(), delta);
		temp = new Kelvin((float)373.15);
		convert = temp.toFahrenheit();
		assertEquals((float)212, convert.getValue(), delta);
	}
	
	public void testKelvintoKelvin ()
	{
		Kelvin temp = new Kelvin ((float)0);
		Temperature convert = temp.toKelvin ();
		assertEquals ((float)0,convert.getValue(), delta);
		temp = new Kelvin ((float)150);
		convert = temp.toKelvin();
		assertEquals ((float)150,convert.getValue(),delta);
	}
}
