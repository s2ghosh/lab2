/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (cs110war): write class javadoc 
 *
 * @author cs110war
 *
 */
public class Kelvin extends Temperature {

	public Kelvin (float t)
	{
		super (t);
		
	}
	
	public String toString ()
	{
		String s = "";
		s += getValue();
		s += " K";
		return s;
	}
	
	public Temperature toCelsius(){
		float kelvin = this.getValue() ;
		float celsius = (float) (kelvin - (float)(273.15));
		return new Celsius (celsius);
		
	}
	
	public Temperature toFahrenheit () {
		float kelvin = this.getValue();
		float celsius = (float) (kelvin - (float)(273.15));
		float fahrenheit = (float)((((float)9/5) * celsius) + 32);
		return new Fahrenheit (fahrenheit);
	}

	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return this;
	}
}
